#include <stdio.h>
#include <windows.h>

typedef int(__stdcall *pFunc)(HWND, LPCSTR, LPCSTR, UINT);

int main(void){
	HMODULE dll = LoadLibrary("legit-dll.dll");
	if (dll != NULL){
		printf("\n[*] Loaded the legit DLL.\n");
		pFunc lpFunc = (pFunc)GetProcAddress(dll, "message");
		int ret = lpFunc(NULL, NULL, NULL, NULL);

	}
	else {
		printf("\n[-] Failed to load the DLL.\n");
	}
	return 0;
}