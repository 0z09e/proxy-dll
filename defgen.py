import pefile
import sys



target = sys.argv[1]

dll = pefile.PE(target)

print("EXPORTS")

for export in dll.DIRECTORY_ENTRY_EXPORT.symbols:
    if export.name:
        print(f"    {export.name.decode()}=legit-dll-original.{export.name.decode()} @{export.ordinal}")