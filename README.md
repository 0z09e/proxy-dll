# Proxy DLL Hijacking
***
## General Execution of the Binary
In this repository I have written source code for 2 DLLs and one binary. Both the `executable-source.c` and `legit-dll.c` are as the name suggests an executable and a dll source code. 
Here the compiled version of `executable-source.c` binary loads the `legit-dll.dll` which is the compiled version of `legit-dll.c` and executes the `message` function from the DLL.

To test that first we compile both the executable and the `DLL` using `x86_64-w64-mingw32-gcc-win32`.

```bash
x86_64-w64-mingw32-gcc-win32 executable-source.c -o executable-binary.exe
x86_64-w64-mingw32-gcc-win32 --shared legit-dll.c -o legit-dll.dll
```

Once the project has been compiled successfully, we transfer both of them to our Windows 10 VM and execute the `executable-binary.exe`.

![](img/Pasted%20image%2020220908001440.png)

As we can see the `DLL` file is getting loaded.
In a real life scinario we will have no idea about what DLLs is getting loaded by a binary. To know about all the DLLs being used by a binary we can use a tool called [Procmon](https://docs.microsoft.com/en-us/sysinternals/downloads/procmon) which is a native windows sysinternals binary to list all the details about a process.
First we run `procmon` on the host and then execute the `executable-binary.exe` binary again to capture the process info in `procmon`.
After the successful execution we go back to `procmon` and add a filter for `Process Name` and set the value to `executable-binary.exe`

![](img/Pasted%20image%2020220908002442.png)

After that, we apply the filter and check all the `Operations`, Here we can add another filter for results to filter `NAME NOT FOUND` results, using which we can list all the missing DLLs and use them for `Privilege Escalation`. But as we are more focused towards persistance, we look for custom DLLs which the binary had successfully loaded during the execution.

![](img/Pasted%20image%2020220908002953.png)

As we can see the binary had successfully loaded the `legit-dll.dll` during the execution.
We can target this one for the persistence deployment via DLL Hijacking.

## DLL Hijacking
Now as our target is to deploy a persistence and not to crash the application, we can Proxy to the original DLL using the malicious DLL.
To do that first we use the `legit-dll.dll` file and dump all the functions present on that DLL using the `defgen.py`.

![](img/Pasted%20image%2020220908003428.png)

We save the output into `legit-dll.def` file.
After that we compile the `malicious-dll.c` file with the `legit-dll.def` file.

```bash
x86_64-w64-mingw32-gcc-win32 --shared malicious-dll.c legit-dll.def -o malicious-dll.dll
```

Hereafter, we transfer the `malicious-dll.dll` to the Windows 10 vm.
On the Windows 10 VM, we first rename the `legit-dll.dll` to `legit-dll-original.dll` and the `malicious-dll.dll` to `legit-dll.dll`.

```bat
move legit-dll.dll legit-dll-original.dll
move malicious-dll.dll legit-dll.dll
```

Here are the contents of the directory after the final step.

![](img/Pasted%20image%2020220908004825.png)

Now if we execute the `executable-binary.exe` we see a popup saying `Hello, I'm a malicious DLL` followed by those to legit messages.

![](img/Pasted%20image%2020220908005050.png)

![](img/Pasted%20image%2020220908005120.png)

This clearly indicates that the binary isn't getting crashed even after the execution of the malicious DLL.
***
